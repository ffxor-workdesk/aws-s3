# Amazon S3 storage sample

This sample demonstrates interaction with [Amazon](https://aws.amazon.com/ru/) S3 storage.

Functionality includes:
- **Browsing** buckets and directories;
- **Downloading** files;
- **Uploading** files;
- **Removing** files;

To get access you should specify `AccessKey` and `SecretKey`, provided by your account on [Amazon](https://aws.amazon.com/ru/).