﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Threading.Tasks;
using System.Security.Claims;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Authorization;
using Amazon.S3;
using Amazon.S3.Model;
using Microsoft.AspNetCore.Http;

namespace AWSS3.Controllers
{
    /// <summary>
    /// Provides interaction with Amazon S3 storage
    /// </summary>
    [Authorize]
    [ExceptionHandler]
    public class BucketsController : Controller
    {
        protected AmazonS3Client client;

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var accessKey = User.Identity.Name;
            var secretKey = User.Claims.First(c => c.Type == ClaimTypes.Authentication).Value;

            this.client = new AmazonS3Client(accessKey, secretKey, Amazon.RegionEndpoint.USEast1);

            base.OnActionExecuting(filterContext);
        }

        public async Task<IActionResult> Index()
        {
            var listBucketsResponse = await this.client.ListBucketsAsync();
            List<S3Bucket> buckets = listBucketsResponse.Buckets;
            return View(buckets);
        }

        /// <summary>
        /// Returns contents of the bucket or nested directory 
        /// </summary>
        /// <param name="bucket">Bucket name</param>
        /// <param name="prefix">Directory path</param>
        /// <returns></returns>
        [Route("[controller]/{bucket}/{*prefix}")]
        public async Task<IActionResult> Contents(string bucket, string prefix)
        {
            ListObjectsRequest request = new ListObjectsRequest()
            {
                BucketName = bucket,
                Delimiter = "/",
                Prefix = prefix
            };

            ListObjectsResponse response = await this.client.ListObjectsAsync(request);

            ViewBag.Prefix = prefix;
            return View(response);
        }

        /// <summary>
        /// Gets file from the nested directory of bucket
        /// </summary>
        /// <param name="bucket">Bucket name</param>
        /// <param name="key">File path</param>
        /// <returns></returns>
        [Route("get/[controller]/{bucket}/{*key}")]
        public async Task<IActionResult> GetObject(string bucket, string key)
        {
            GetObjectResponse response = await this.client.GetObjectAsync(new GetObjectRequest()
            {
                BucketName = bucket,
                Key = key
            });

            return File(response.ResponseStream, "application/octet-stream", key);
        }

        /// <summary>
        /// Removes file from the nested directory of bucket
        /// </summary>
        /// <param name="bucket">Bucket name</param>
        /// <param name="key">File path</param>
        /// <returns></returns>
        [Route("remove/[controller]/{bucket}/{*key}")]
        public async Task<IActionResult> Remove(string bucket, string key)
        {
            await this.client.DeleteObjectAsync(new DeleteObjectRequest()
            {
                BucketName = bucket,
                Key = key
            });

            return RedirectToAction("Contents", null, new { bucket, prefix = Path.GetDirectoryName(key) + "/" });
        }

        /// <summary>
        /// Uploads file to the bucket or nested directory
        /// </summary>
        /// <param name="bucket">Bucket name</param>
        /// <param name="prefix">Directory path</param>
        /// <param name="FileForUpload">Uploaded file</param>
        /// <returns></returns>
        [HttpPost]
        [Route("upload/[controller]/{bucket}/{*prefix}")]
        public async Task<IActionResult> Upload(string bucket, string prefix, IFormFile FileForUpload)
        {
            string directory = Path.GetTempPath() + Guid.NewGuid();
            if (!Directory.Exists(directory))
                Directory.CreateDirectory(directory);

            string fileName = Path.GetFileName(FileForUpload.FileName);
            string pathForFile = Path.Combine(directory, fileName);

            using (var fileStream = new FileStream(pathForFile, FileMode.Create))
            {
                await FileForUpload.CopyToAsync(fileStream);
            }

            await this.client.PutObjectAsync(new PutObjectRequest()
            {
                BucketName = bucket,
                Key = String.Concat(prefix, fileName),
                FilePath = pathForFile,
                ContentType = FileForUpload.ContentType
            });

            System.IO.File.Delete(pathForFile);

            return RedirectToAction("Contents", null, new { bucket, prefix });
        }

        /// <summary>
        /// Shows info about the file
        /// </summary>
        /// <param name="bucket">Bucket name</param>
        /// <param name="key">File path</param>
        /// <returns></returns>
        [Route("info/[controller]/{bucket}/{*key}")]
        public async Task<IActionResult> Info(string bucket, string key)
        {
            GetObjectMetadataResponse response = await this.client.GetObjectMetadataAsync(new GetObjectMetadataRequest()
            {
                BucketName = bucket,
                Key = key
            });

            ViewBag.BucketName = bucket;
            ViewBag.Name = key;

            return View(response);
        }
    }

    /// <summary>
    /// Exception handler filter to provide error info in string
    /// </summary>
    public class ExceptionHandlerAttribute : Attribute, IExceptionFilter
    {
        public void OnException(ExceptionContext context)
        {
            context.Result = new ContentResult()
            {
                Content = $"Runtime ERROR: {context.Exception.Message}"
            };
            context.HttpContext.Response.StatusCode = 500;
        }
    }
}
